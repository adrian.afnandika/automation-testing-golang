.ONESHELL:
SHELL=/bin/sh

.PHONY: run
run:
	@# Help: Run the Go service.

	go run main.go

.PHONY: test
test:
	@# Help: Run the go test.

	go test -v -race -cover ./... > test.out