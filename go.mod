module automation-testing-golang

go 1.17

require (
	github.com/boumenot/gocover-cobertura v1.2.0 // indirect
	github.com/ggere/gototal-cobertura v1.0.2 // indirect
	golang.org/x/tools v0.0.0-20200526224456-8b020aee10d2 // indirect
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543 // indirect
)
